### tic-tac-toe

To get started: 

- clone this project
- yarn install
- change host url for production at .env.production (that's where the socket will try to connect to)
- use yarn server-dev and yarn client-dev to run app in dev env
- use yarn server to run for production, but don't forget to build client app first